// Code from https://www.youtube.com/playlist?list=PL4cUxeGkcC9gTxqJBcDmoi5Q2pzDusSL7
// assets variable contain the home page assets used when accessing the site offline
// staticCacheName and dynamicCacheName has version for cache versioning

const staticCacheName = 'site-static-v1';
const dynamicCacheName = 'site-dynamic-v1';
const assets = [
    '/',
    '/index.html',
    '/fallback.html',
    '/styles/style.css',
    '/styles/index.css',
    '/scripts/main.js',
    '/scripts/index.js',
    '/images/favicon.png',
    '/images/index/burger.jpg',
    '/images/index/burgerExclusive.png',
    '/images/index/drink.jpg',
    '/images/index/drinkExclusive.png',
    '/images/index/fries.jpg',
    '/images/index/greeksalad.jpg',
    '/images/index/pasta.jpg',
    '/images/index/pizza.jpg',
    '/images/index/pizzaExclusive.png',
    '/images/index/slide1.jpg',
    '/images/index/slide2.jpg',
    '/images/index/slide3.jpg'
];

//Installing Service worker and precaching the home page assets.
self.addEventListener('install', evt => {
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('caching shell assets');
            cache.addAll(assets);
        })
    );
});

//Activate event and to remove old caches from the browser when there is an update on the website.
self.addEventListener('activate', evt => {
    evt.waitUntil(
        caches.keys().then(keys =>{
            return Promise.all(keys
                .filter(key => key !== staticCacheName && key !== dynamicCacheName)
                .map(key => caches.delete(key))    
            )
        })
    );
});

//Fetching event and cached assets so the website can be accessed offline. 
//Also fills the dynamicCacheName variable to automatically precache assets on pages when accessed online.
//Also shows a fallback page when an uncached page is accessed offline.
self.addEventListener('fetch', evt => {
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request).then(fetchRes => {
                return caches.open(dynamicCacheName).then(cache => {
                   cache.put(evt.request.url, fetchRes.clone());
                   return fetchRes;
                })
            });
        }).catch(() => {
            if(evt.request.url.indexOf('.html') > -1){
                return caches.match('/fallback.html');
            }         
        })
    );
});