var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//Took help from: https://reactjs.org/docs/forms.html

//Declaring form
function Form() {
  var _React$useState = React.useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      submitted = _React$useState2[0],
      setSubmitted = _React$useState2[1];
  //setting states


  var _React$useState3 = React.useState({
    name: "",
    email: "",
    message: ""
  }),
      _React$useState4 = _slicedToArray(_React$useState3, 2),
      state = _React$useState4[0],
      setState = _React$useState4[1];
  //


  function handleChange(event) {
    var name = event.target.name;
    var value = event.target.value;
    setState(Object.assign({}, state, _defineProperty({}, name, value)));
  }
  //if submitted form it wll execute
  function handleSubmit(event) {
    event.preventDefault();

    // Submit end logic
    setSubmitted(true);
  }

  //Writing form script in js
  return React.createElement(
    "form",
    { id: "contactForm", onSubmit: handleSubmit },
    React.createElement(
      "div",
      { className: "form-group textAreaSize" },
      React.createElement(
        "label",
        null,
        "Name:"
      ),
      React.createElement("input", { id: "contact-name", placeholder: "Your Name", className: "form-control", type: "text", name: "name", onChange: handleChange })
    ),
    React.createElement(
      "div",
      { className: "form-group textAreaSize" },
      React.createElement(
        "label",
        null,
        "Email:"
      ),
      React.createElement("input", { id: "contact-email", placeholder: "Your E-Mail", className: "form-control", type: "email", name: "email", onChange: handleChange })
    ),
    React.createElement(
      "div",
      { className: "form-group textAreaSize" },
      React.createElement(
        "label",
        null,
        "Query:"
      ),
      React.createElement("textarea", { placeholder: "Your Query", name: "message", onChange: handleChange })
    ),
    React.createElement(
      "div",
      null,
      submitted ? React.createElement(
        "h5",
        null,
        "Your message was submitted successfully!"
      ) : React.createElement("input", { className: "btnSubmit", type: "submit", value: "Submit" })
    )
  );
}

var domContainer = document.querySelector("#form_container");
ReactDOM.render(React.createElement(Form, null), domContainer);