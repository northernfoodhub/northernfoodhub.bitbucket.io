// Filter By (From: https://www.w3schools.com/howto/howto_js_filter_elements.asp)

filterToggle("all")

function filterToggle(criteria) {
    /* By default, all items are shown. */
    if (criteria == "all") {
        criteria = "";
    }

    var itemList, item

    itemList = document.getElementsByClassName("filter-out");
    
    for (item = 0; item < itemList.length; item++) {
        removeItem(itemList[item], "show"); // initially removes all cards
        if (itemList[item].className.indexOf(criteria) > -1) { // adds cards that have the specified criteria (sides, drinks etc.) as a class
            addItem(itemList[item], "show");
        }
    }
}

// Show Items

function addItem(item, action) {
    var filterInList, actionList, i

    filterInList = item.className.split(" ");
    actionList = action.split(" ");

    for (i = 0; i < actionList.length; i++) {
        if (filterInList.indexOf(actionList[i]) == -1) {
            item.className += " " + actionList[i]; // This adds the specified class (in this case, "show") to the card.
        }
    }
}

// Hide Items

function removeItem(item, action) {
    var filterOutList, actionList, i

    filterOutList = item.className.split(" ");
    actionList = action.split(" ");

    for (i = 0; i < actionList.length; i++) {
        while (filterOutList.indexOf(actionList[i]) > -1) {
            filterOutList.splice(filterOutList.indexOf(actionList[i]), 1); // This removes the specified class (in this case, "show") from the card.
        }
    }

    item.className = filterOutList.join(" ")
}