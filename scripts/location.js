//Putting all address map links in an array
let addresses = [
    document.getElementById('nswAddress1'),
    document.getElementById('nswAddress2'),
    document.getElementById('ntAddress1'),
    document.getElementById('ntAddress2'),
    document.getElementById('vicAddress1'),
    document.getElementById('vicAddress2'),
    document.getElementById('waAddress1'),
    document.getElementById('waAddress2')
    ];

//Initialising views count
countNSW = 0;
countNT = 0;
countVIC = 0;
countWA = 0;

//Will run the viewCount function if any of the address map links are clicked
for (let i=0; i < addresses.length; i++) {
    addresses[i].addEventListener('click', viewCount);
}

//function that will increase view count per state everytime the address map links are clicked
function viewCount() {
    if (event.target.id === 'nswAddress1' || event.target.id === 'nswAddress2') {
        countNSW += 1;
        viewsNSW.innerHTML = `<ion-icon name="eye"></ion-icon> ` + countNSW + " View(s)";
    } else if (event.target.id === 'ntAddress1' || event.target.id === 'ntAddress2') {
        countNT += 1;
        viewsNT.innerHTML = `<ion-icon name="eye"></ion-icon> ` + countNT + " View(s)";
    } else if (event.target.id === 'vicAddress1' || event.target.id === 'vicAddress2') {
        countVIC += 1;
        viewsVIC.innerHTML = `<ion-icon name="eye"></ion-icon> ` + countVIC + " View(s)";
    } else if (event.target.id === 'waAddress1' || event.target.id === 'waAddress2') {
        countWA += 1;
        viewsWA.innerHTML = `<ion-icon name="eye"></ion-icon> ` + countWA + " View(s)";
    }
}


function myFunction() {
    var txt;
    var person = prompt("Please enter your comments:");

    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        txt = "Thanks for your feedback!";
    }
    document.getElementById("demo").innerHTML = txt;
}

function myFunction1() {
    var txt;
    var person = prompt("Please enter your comments:");

    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        txt = "Thanks for your feedback!";
    }

    document.getElementById("demo").innerHTML = txt;
}

function myFunction2() {
    var txt;
    var person = prompt("Please enter your comments:");

    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        txt = "Thanks for your feedback!";
    }
    document.getElementById("demo").innerHTML = txt;
}

function myFunction3() {
    var txt;
    var person = prompt("Please enter your comments:");
    
    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        txt = "Thanks for your feedback!";
    }
    document.getElementById("demo").innerHTML = txt;
}
