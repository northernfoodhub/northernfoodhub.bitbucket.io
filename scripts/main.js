// Mobile View Menu Toggle

const menuAction = document.getElementsByClassName('menu-toggle')[0];
menuAction.addEventListener('click', navToggle);

// Show/Hide Mobile Navigation Links
function navToggle(){
    const navLinks = document.getElementsByClassName('top-nav')[0];
    navLinks.classList.toggle('mobile-nav-links');
}

// Code from https://bootsnipp.com/snippets/lV7xM
//The scrollTop property sets or returns the number of pixels an element's content is scrolled vertically.
$(window).on("scroll", function() {
    if ($(this).scrollTop() > 10) { //if scroll down then shows scroll button and its css
        $("nav.navbar").addClass("mybg-dark");
        $("nav.navbar").addClass("navbar-shrink");
        $(".navbar-dark .navbar-toggler").css({"background-color" : "#212529"}); //setting the css of the background color of scroll top

    } else { // if scroll back to the top then remove the scroller
        $("nav.navbar").removeClass("mybg-dark");
        $("nav.navbar").removeClass("navbar-shrink");
        $(".navbar-brand").css({
            "color": "#fff"
        });
        $(".navbar-dark .navbar-toggler").css({"background-color" : "transparent"});
    }
});
$(document).ready(function() {

    $(function() {

        $(document).on('scroll', function() {

            //show the scroll top button if scroll top is scrolled higher than 100
            if ($(window).scrollTop() > 100) {
                $('.scroll-top-wrapper').addClass('show');
            } else { // else removes the scroll top
                $('.scroll-top-wrapper').removeClass('show');
            }
        });

        //scrolling back to the top if clicked.
        $('.scroll-top-wrapper').on('click', scrollToTop);
    });

    //scrolling back to the top
    function scrollToTop() {
        verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
        element = $('body');
        offset = element.offset();
        offsetTop = offset.top;
        $('html, body').animate({
            scrollTop: offsetTop
        }, 500, 'linear');
    }

});

// Registering Service Worker for PWA
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js')
        .then((reg) => console.log('Service worker registered', reg))
        .catch((err) => console.log('Service worker not registered', err));
}

