/*** Generate meal based on option selected***/ 

//Putting all the meal item elements to a variable
const mealClassicBurger = document.getElementById('mealClassicBurger');
const mealSpaghettiBolognese = document.getElementById('mealSpaghettiBolognese');
const mealClassicChips = document.getElementById('mealClassicChips');
const mealGreekSalad = document.getElementById('mealGreekSalad');
const mealIcedTea = document.getElementById('mealIcedTea');

//Putting all the meal items in an array
let allMealItems= [mealClassicBurger, mealSpaghettiBolognese, mealClassicChips, mealGreekSalad, mealIcedTea];

//Will run the showMeal function if the surprisebutton is clicked
document.getElementById('surpriseButton').addEventListener('click', showMeal);

//This function will show the meal when the surprise button is clicked
function showMeal() {
    var threeRandomNumbers = generateThreeRandomNumbers(); //Assign to an array variable the three random numbers generated from the function generateThreeRandomNumbers()
    let surpriseMeal = [allMealItems[threeRandomNumbers[0]], allMealItems[threeRandomNumbers[1]], allMealItems[threeRandomNumbers[2]]]; // this will grab 3 random meal items from the menu

    document.getElementById('placeholderMeal').className = "yourMealHide"; // removes the placeholder question mark images
 
    //This for loop will hide all meal items on button clicked so only 3 meal items are shown everytime
    for (let i=0; i < allMealItems.length; i++) {
        allMealItems[i].className = "col yourMealHide";   
    }

    //This for loop will randomly show 3 meal items everytime
    for (let i=0; i < surpriseMeal.length; i++) {
        surpriseMeal[i].className = "col";   
    }
}

//This function will add 3 UNIQUE random numbers to the set then convert the set to an array and return the array
function generateThreeRandomNumbers() {
    const threeRandomNumbersSet = new Set();
    while(threeRandomNumbersSet.size < 3) {
        threeRandomNumbersSet.add(Math.floor(Math.random() * allMealItems.length)); //Generate a random number from 0 to the length of allMealItems array
    }
    threeRandomNumbers = Array.from(threeRandomNumbersSet); //convert set to array
    return threeRandomNumbers;
}
