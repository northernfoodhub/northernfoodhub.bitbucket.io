class PurchaseButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { purchased: false };
  }

  render() {
    //If the button is cicked then it will be on the purchased state
    if (this.state.purchased) {
      return "Your items has been purchased";
    }

    //keep rendering it (and show button) until the above if condition is true
    return <button onClick={() => this.setState({ purchased: true }) }>Purchase</button>;
  }
}

let domContainer = document.querySelector("#purchase_button_container");
ReactDOM.render(<PurchaseButton />, domContainer);
