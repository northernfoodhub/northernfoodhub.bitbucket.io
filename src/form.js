//Took help from: https://reactjs.org/docs/forms.html

//Declaring form
function Form() {
  const [submitted, setSubmitted] = React.useState(false);
  //setting states
  const [state, setState] = React.useState({
    name: "",
    email: "",
    message: "",
  });
  //
  function handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    setState({ ...state, [name]: value });
  }
  //if submitted form it wll execute
  function handleSubmit(event) {
    event.preventDefault();


    // Submit end logic
    setSubmitted(true);
  }

  //Writing form script in js
  return (
    <form id="contactForm" onSubmit={handleSubmit}>
      <div className = "form-group textAreaSize">
        <label>
          Name:
        </label>
        <input id = "contact-name" placeholder="Your Name" className="form-control" type="text" name="name" onChange={handleChange} />
      </div>


      <div className = "form-group textAreaSize">
        <label>
          Email:
        </label>
        <input id="contact-email" placeholder="Your E-Mail" className="form-control" type="email" name="email" onChange={handleChange} />
      </div>

      <div className = "form-group textAreaSize">
        <label>
          Query:
        </label>
        <textarea placeholder="Your Query" name="message" onChange={handleChange} />
      </div>

      <div>
        {submitted ? (
          <h5>Your message was submitted successfully!</h5>
        ) : (
          <input className="btnSubmit" type="submit" value="Submit" />
        )}
      </div>
    </form>
  );
}

let domContainer = document.querySelector("#form_container");
ReactDOM.render(<Form />, domContainer);
